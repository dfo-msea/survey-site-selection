# Survey site selection

__Main authors:__  Patrick Thompson and Emily Rubidge
__Affiliation:__  Fisheries and Oceans Canada (DFO)   
__Group:__        Marine Spatial Ecology and Analysis   
__Location:__     Pacific Science Enterprise Centre  
__Contact:__      e-mail: patrick.thompson@dfo-mpo.gc.ca or emily.rubidge@dfo-mpo.gc.ca 


- [Objective](#objective)
- [Status](#status)
- [Contents](#contents)
- [Methods](#methods)
- [Requirements](#requirements)
- [Caveats](#caveats)
- [References](#references)


## Objective
This repo contains code and documented protocols used for selecting survey sites using the Balanced Acceptance Sampling method [(van Dam-Bates et al. 2018)](https://doi.org/10.1111/2041-210X.13003).

## Status
Ongoing.

## Contents
This repo contains subfolders for individual surveys. The year of the survey is included in the file name.

## Methods
The sites are selected using the Balanced Acceptance Sampling (BAS) method [(van Dam-Bates et al. 2018)](https://doi.org/10.1111/2041-210X.13003). This uses the [DFO-master-sample package](https://github.com/paul-vdb/DFO-master-sample/) that was developed by Paul van Dam-Bates through a contract. The DFO-master-sample package can be installed using the following code:

devtools::install_github("paul-vdb/DFO-master-sample")


Two applications of the BAS are applied depending on the type of survey:  

1. Sampling of *continous areas* (e.g., open water) where all locations are equally valid sampling points uses the **standard BAS method**. This method ensures that surveys are spatially balanced and that sample points are coordinated across any survey within British Columbia (see van Dam-Bates 2018 for details). Survey areas can be stratified (e.g., 20 sites within an MPA, 10 sites outside of an MPA) to allow differential allocation of sample effort in different areas of interest. Note, that breaks in the continuous space (e.g., land) will reduce the spatial balance. This may be a problem when selecting sites in Fjords, where most of the area is land or in areas with lots of islands. One option, is to stratify by subsets of sampleable space. This will ensure spatial balance within those areas, but not necessarily across areas. 

2. Selecting sites along *linear features* such as shorelines uses the **Halton box method**. This method breaks the linear feature up into 100 m boxes and then selects boxes which should be sampled. The selected boxes will be spatially balanced across the sampleable area, but will not be spatially balanced across continuous space. For example, if most shoreline is on the eastern side of the sampling domain, but there are a few small islands on the west side, more sites will be selected from the eastern side. In this method, it is critical to only include sampleable sites in the inputs to the methods. This is because the method selects a balanced set of sites across all potential sites. If sites are removed (e.g., because they are unsampleable) this will affect the spatial balance. Thus, sites that cannot be accessed or that are too dangerous to sample should be identified and removed from the shape file of the potential sites before running the BAS Halton box method. Stratification can be done to ensure that sampling effort is allocated based on interest in different habitat features or types. For example, if an area contains mostly rocky shoreline, but the researcher wants to ensure that sandy or muddy sites are also sampled, they could stratify by substrate type. Then they might select 15 rocky sites, 10 sandy sites, and 5 muddy sites.In this method, sites would be balanced for each substrate type, but not across substrate types. It is also important to note that, when stratifying, the method cannot account for uncertainty in the site type. Therefore, if a site is labelled as rock in the substrate layer but ends up being sand when the researchers go to the site, this will disrupt the spatial balance of the survey. In this case, the researchers could choose to sample the site anyway, but mark it as sand. This will reduce the spatial balance of both the rocky and sandy sites. They could choose not to sample the site. This would reduce the spatial balance of the rocky sites, but not the sandy sites. They could choose to select a nearby rocky site to sample. This would somewhat reduce the spatial balance of the rocky sites, but would not affect the balance of the sandy sites. It is recommended that in such a case, that the researchers consider whether maintaining spatial balance is more important than sampling in the selected locations, and how important it is that they sample the substrate types in the predetermined proportions. Logistics should also be considered. If changing sites would result in missing a low tide sampling opportunity, the researchers are recommended to sample anyway as getting samples is likely more important than maintaining perfect spatial balance. 

## Requirements
### Standard BAS
1. The survey domain as a polygon shape file. 
2. Any features over which the sample points should be surveyed (e.g., MPAs)

### Halton Box
1. The survey domain as a polygon shape file. 
2. The linear feature from which sites should be selected as a line shape file. 
3. Any features over which the sample points should be surveyed (e.g. substrate types, MPAs)
4. Unsampleable areas (either removed from the linear feature of potential sites, or as a polygon which can be used to subset the linear feature).

## Caveats
The method cannot account for uncertainty in the spatial layers provided. See text for the Halton Box methods for more details.


## References 
[van Dam-Bates, P., O. Gansell, and B. Robinson. 2018. Using balanced acceptance sampling as a master sample for environmental surveys. Methods in Ecology and Evolution. 9: 1718-1726.](https://doi.org/10.1111/2041-210X.13003).
