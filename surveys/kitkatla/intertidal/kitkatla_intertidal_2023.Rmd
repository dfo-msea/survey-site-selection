---
title: "Kitkatla intertidal site selection for 2023 Vector survey"
author: "Patrick Thompson"
date: "`r Sys.Date()`"
output:
    bookdown::html_document2:
      toc: false
      number_sections: false
      fig_caption: true
      keep_tex: false
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(warning = FALSE, message = FALSE) 
```

```{r, echo=FALSE, warnings = FALSE}
library(BASMasterSample)
library(sf)
library(tidyverse)
library(raster)
library(data.table)
library(knitr)

source('../../../functions/HSS.r') #Halton box functions not included in BASMasterSample package
```

## Objective
Select a set of intertidal sites in Kitkatla proposed MPA for sampling across multiple years. 

## Methods
```{r loaddata, echo = FALSE, results = 'hide'}
studydomain <- st_read("../kitkatla_data/kitkatla_domain/Kitkatla.shp")
studydomain <- st_zm(studydomain)
studydomain <- st_transform(studydomain, crs = "epsg:3005")

coastline <- st_read("../../../data/Coastline_low_res/BC-PacNW_BCAlbers.shp")

coastalclasses <- st_read("../../../data/coastal_classes/mpatt_eco_coarse_coastalclasses_data_ForSiteSelection.shp")

no_go <- st_read("../kitkatla_data/kitkatla_nogo/KitkatlaNoGoZones.shp") %>% st_transform(crs = "epsg:3005")
```
### Selecting potential sites
Potential sites were obtained from the coastal classes database, clipped to the extent of the Kitkatla MPA polygons. Areas that are not safely accessible by small boat and areas that were identified as being unsafe to sample were identified using satellite imagery. Polygons that exclude these areas were produced using QGIS.

```{r processdata, echo = FALSE, warnings = 'hide'}
BAS_extent <- studydomain[,]
BAS_extent$include <- TRUE

kitkatla_coastal <- st_intersection(coastalclasses, BAS_extent %>% dplyr::select(include)) #add coastal classes
kitkatla_coastal <- st_difference(kitkatla_coastal, no_go %>% st_union()) #exclude areas that can't be safely accessed or sampled

#collapse coastal classes to mud, rock, sand, or gravel
kitkatla_coastal <- kitkatla_coastal %>% 
  filter(include == TRUE) %>% 
  mutate(GroupedClass = ifelse(MainClass %in% c("Estuary (Organics/Fines)", "Mud Flat"),"Mud",
                               ifelse(MainClass %in% c("Rock Platform", "Rock Ramp"), "Rock", 
                                      ifelse(MainClass %in% c("Sand Beach", "Sand Flat"), "Sand",
                                             ifelse(MainClass %in% c("Gravel Beach", "Gravel Flat", "Sand and Gravel Beach", "Sand and Gravel Flat or Fan"), "Gravel", NA)))))
```


```{r substratetable, echo = FALSE}
kitkatla_coastal %>% 
  st_drop_geometry() %>% 
  tibble() %>% 
  dplyr::select(`Coastal class` = MainClass, `Grouped class` = GroupedClass) %>% 
  unique() %>% 
  arrange(`Grouped class`) %>% 
  kable(caption = "Coastal classes vs. grouped classes used in the analysis.")
```

```{r candsites, fig.align='center', fig.height=6, fig.width=6, fig.cap="Candidate sites by substrate type. The Kitkatla proposed MPA areas (i.e., study domain) are shown in light blue.", echo = FALSE}
kitkatla_coastal_selected <- kitkatla_coastal %>% 
  filter(!is.na(GroupedClass))

kitkatla_coastal_selected <- st_intersection(kitkatla_coastal_selected, studydomain)

kitkatla_coastal_sub <- st_intersection(kitkatla_coastal_selected, y = studydomain) %>% 
  st_cast(to = "MULTIPOINT")

all_candidate_points <- st_cast(kitkatla_coastal_sub, "POINT")

ggplot(kitkatla_coastal_sub)+
  geom_sf(data = BAS_extent, fill = "lightblue")+
  geom_sf(data = st_crop(coastline, BAS_extent))+
  geom_sf(aes(color = GroupedClass), size = 0.8)+
  scale_color_brewer(palette = "Set1", "")+
  theme_bw()+
    ylab("")+
  xlab("")+
  coord_sf(expand = FALSE)+
  guides(colour = guide_legend(override.aes = list(size=3)))
```

The coastal classes database contains more detailed substrate information than was desirable for site selection. Therefore, we combined coastal classes that corresponded to four general substrate types: rock, sand, mud, and gravel (Table \@ref(tab:substratetable)). Some classes were identified as not being of interest (undefined, man made, channel) or unsampleable (rock cliff) and these were excluded from the potential sites. Sites were selected based on Halton boxes that are 100 m in length and width. This 100 m box defines the potential sampleable habitat, but researchers will need to decide how to sample within that box. Because a Halton box is 100 m in size, it can contain multiple substrate types. Therefore, we elected to define boxes that include any sand or mud as sand or mud. If no sand or mud exists, but gravel is present, we defined the box as gravel. Only boxes that only included rock were defined as rock. When sampling, researchers should try to sample areas of the Halton box that correspond with the defined substrate. The final list of candidate sites are shown in Figure \@ref(fig:candsites). `r round(table(all_candidate_points$GroupedClass)[3]/nrow(all_candidate_points)*100)`% of candidate sites are rock, `r round(table(all_candidate_points$GroupedClass)[4]/nrow(all_candidate_points)*100)`% are sand, `r round(table(all_candidate_points$GroupedClass)[2]/nrow(all_candidate_points)*100)`% are mud, and `r round(table(all_candidate_points$GroupedClass)[1]/nrow(all_candidate_points)*100)`% are gravel.

```{r HIPfunction, echo = FALSE}
habitat_HIP <- function(habitats, samples, priority_habitats = NA){
  #create Halton boxes#####
  #get bounding box for BC
  # MARINE MS, need to add the seed to it for it to work.
  bb <- getBB()
  attr(bb, "seed") <- getSeed()
  
  all_boxes <- point2Frame(pts =  kitkatla_coastal_sub %>% filter(GroupedClass %in% habitats), bb = bb, size = 100)
  
  if(!is.na(priority_habitats[1])){
    all_boxes_priority <- point2Frame(pts = kitkatla_coastal_sub %>% filter(GroupedClass %in% priority_habitats), bb = bb, size = 100)
    all_boxes <- all_boxes %>% filter(!(HaltonIndex %in% all_boxes_priority$HaltonIndex))
  }
  
  coords <- st_coordinates(st_centroid(all_boxes))
  bb.tmp <- st_bbox(bb)
  bbox <- cbind(c(bb.tmp['xmin'], bb.tmp['ymin']), c(bb.tmp['xmax'], bb.tmp['ymax']))
  
  HSS.pts <- getHipSample(X = coords[,1], Y = coords[,2], index = all_boxes$HaltonIndex,
                          N = samples*2, bb = bbox,  base = c(2,3), quiet = TRUE,
                          Ps1 = 0:1, Ps2 = 0:2, hipS1 = 0:1, hipS2 = c(0,2,1))	# Changed the order for fun
  n.boxes <- length(table(HSS.pts$HIPIndex))
  
  # Chooses a sample from all boxes. Small wrapper function.
  pts.new <- getSamples(HSS.pts, n = samples)
  # Bounding box to clip the HIP boxes to.
  bb.tmp <- st_bbox(BAS_extent)
  bbox2 <- cbind(c(bb.tmp['xmin'], bb.tmp['ymin']), c(bb.tmp['xmax'], bb.tmp['ymax']))
  HIPBoxes <- st_as_sf(getHIPBoxes(hip = HSS.pts, bb = bbox2, n = n.boxes, base = c(2,3)))
  HIPBoxes <- st_set_crs(HIPBoxes, value = st_crs(bb))
  
  #plot(HIPBoxes)
  
  selected_boxes <- all_boxes %>% filter(HaltonIndex %in% pts.new$index)
  
  selected_boxes_points <- st_centroid(selected_boxes)
  
  
  #selected_boxes <- st_intersection(habitat_polygons %>%  filter(habitat %in% habitats), selected_boxes)
  return(selected_boxes)
}
```

### Selecting survey sites
We chose to stratify the potential sites based on substrate because of the unequal representation of the different substrate types. For this we selected 15 rocky sites, 13 sand or mud sites, and 12 gravel sites. Within each substrate grouping (but keeping mud and sand together) sites were given an order of sampling based on their Halton Index. An overall order for the region was produced. This order determines which sites should be selected first to sample. However, sampling does not necessarily need to be conducted in this order as long as all of the initial sites in the sequence are sampled at some point. For example, site 3 could be sampled before sites 1 and 2, as long as sites 1 and 2 were sampled at some point. However, if sites in the sequence are skipped entirely this will reduce the spatial balance of the survey plan.

The final list of sites is provided in a csv called "kitkatla_intertidal_2023.csv" and a shape file "kitkatla_intertidal_2023_shp" that are found within the Gitlab repository in the subfolder for this survey. For each selected site, these files specify the Halton index, the substrate class, the overall order, the x and y coordinates of the centroid of the Halton box (UTMs, BC Albers), and the latitude and longitude of the centroid in decimal degrees.


```{r runHIP, echo = FALSE}
BAS_samples_rock <- habitat_HIP(habitats = c("Rock"), priority_habitats = c("Sand", "Gravel", "Mud"), samples = 15)
BAS_samples_rock <- left_join(BAS_samples_rock, BAS_samples_rock %>% 
                                st_intersection(kitkatla_coastal_sub) %>% 
                                filter(GroupedClass == "Rock") %>% 
                                dplyr::select(HaltonIndex, GroupedClass) %>% 
                                group_by(HaltonIndex) %>% 
                                filter(row_number()==1) %>% 
                                ungroup() %>% 
                                arrange(HaltonIndex) %>% 
                                mutate(group_order = 1:15) %>% 
                                st_drop_geometry()) %>% 
    mutate(group = "rock")


BAS_samples_SandMud <- habitat_HIP(habitats = c("Sand", "Mud"), samples = 13)
BAS_samples_SandMud <- left_join(BAS_samples_SandMud, BAS_samples_SandMud %>% 
                                   st_intersection(kitkatla_coastal_sub) %>% 
                                   filter(GroupedClass != "Rock",GroupedClass != "Gravel") %>% 
                                   dplyr::select(HaltonIndex, GroupedClass) %>% 
                                   unique() %>% 
                                   arrange(HaltonIndex) %>% 
                                  mutate(group_order = 1:13) %>% 
                                  st_drop_geometry()) %>% 
    mutate(group = "sand/mud")


BAS_samples_Gravel <- habitat_HIP(habitats = c("Gravel"), samples = 12, priority_habitats = c("Sand", "Mud"))
BAS_samples_Gravel <- left_join(BAS_samples_Gravel, BAS_samples_Gravel %>% 
                                  st_intersection(kitkatla_coastal_sub) %>% 
                                  filter(GroupedClass == "Gravel") %>% 
                                  dplyr::select(HaltonIndex, GroupedClass) %>% 
                                  unique() %>% 
                                  arrange(HaltonIndex) %>% 
                                  mutate(group_order = 1:12) %>% 
                                  st_drop_geometry()) %>% 
  mutate(group = "gravel")

BAS_samples <- bind_rows(BAS_samples_rock, BAS_samples_SandMud, BAS_samples_Gravel) %>% 
  arrange(group, group_order)
```


 

```{r selectedall, fig.align='center', fig.height=6, fig.width=6, fig.cap="Selected sites by substrate type. The Kitkatla proposed MPA areas (i.e., study domain) are shown in light blue.", echo = FALSE}
ggplot(BAS_samples %>% st_cast("POINT"))+
  geom_sf(data = BAS_extent, fill = "lightblue")+
  geom_sf(data = st_crop(coastline, BAS_extent))+
  geom_sf(aes(color = GroupedClass), size = 3)+
  theme_bw()+
  scale_color_brewer(palette = "Set1", "")+
  ylab("")+
  xlab("")+
  coord_sf(expand = FALSE)+
  guides(colour = guide_legend(override.aes = list(size=3)))
```

```{r savedata, echo = FALSE, message=FALSE}
Intertidal_coord <- BAS_samples

Intertidal_coord <- Intertidal_coord %>% 
  st_drop_geometry() %>% 
  dplyr::select(HaltonIndex, substrate = GroupedClass, group_order, group)

XY <- data.frame(st_coordinates(st_centroid(BAS_samples)))

Intertidal_coord$y <- XY$Y
Intertidal_coord$x <- XY$X

latlon <- data.frame(st_coordinates(st_centroid(BAS_samples %>% st_transform(crs = "EPSG:4326"))))

Intertidal_coord$lat <- latlon$Y
Intertidal_coord$lon <- latlon$X

write_csv(Intertidal_coord, "./Kitkatla_intertidal_2023.csv")
if(!dir.exists("Kitkatla_intertidal_2023_shp")){
dir.create(path = "Kitkatla_intertidal_2023_shp")}
st_write(obj = BAS_samples, "./Kitkatla_intertidal_2023_shp/Kitkatla_intertidal_2023.shp", append = TRUE)
```
